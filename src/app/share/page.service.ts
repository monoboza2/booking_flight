import { Injectable } from '@angular/core';
import {Mockbooking} from './mockbooking'
import {Modelbooking} from '../components/booking/modelbooking'
@Injectable({
  providedIn: 'root'
})
export class PageService {
  mbook:Modelbooking[]=[]

  constructor() {
    this.mbook=Mockbooking.mbook
  }

  getBooks():Modelbooking[] {
    return this.mbook
  }

  addBooks(f:Modelbooking){
    this.mbook.push(f)
  }

}
