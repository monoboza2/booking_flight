import { Modelbooking } from "../components/booking/modelbooking";
import {NgbDate} from '@ng-bootstrap/ng-bootstrap'
export class Mockbooking {

  public static mbook:Modelbooking[]=[{
    fullName:"thanathon Yooyen",
    from:"Thailand",
    to:"U.S.A",
    type:"One trip",
    departure:new Date(),
    arrival:new Date(),
    adults: 0,
    children: 0,
    infants: 0,
  }]
}
