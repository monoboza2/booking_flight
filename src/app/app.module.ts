import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BookingComponent } from './components/booking/booking.component';
import{FormsModule,ReactiveFormsModule} from '@angular/forms';
import {PageService} from './share/page.service';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { DatePickerModule } from '@syncfusion/ej2-angular-calendars';
import { MaterialModule } from './material.module'
import { MAT_DATE_LOCALE} from '@angular/material/core';

@NgModule({
  declarations: [
    AppComponent,
    BookingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    DatePickerModule,
    MaterialModule,
    BrowserAnimationsModule,
  ],
  providers: [PageService,{provide:MAT_DATE_LOCALE,useValue:"th-TH"}],
  bootstrap: [AppComponent]
})
export class AppModule { }
