
import {NgbDate, NgbCalendar, NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';

export class Modelbooking {
  fullName: string;
  from: string;
  to: string;
  type: string;
  departure:Date;
  arrival: Date;
  adults: number;
  children: number;
  infants: number;

  constructor(
    fullName: string,
    from: string,
    to: string,
    type: string,
    departure: Date,
    arrival: Date,
    adults: number,
    children: number,
    infants: number,
  ) {
    this.fullName = fullName;
    this.from = from;
    this.to = to;
    this.type = type;
    this.adults = adults;
    this.departure = departure;
    this.children = children;
    this.infants = infants;
    this.arrival = arrival;
  }
}
