import { Component, OnInit } from '@angular/core';
import { Modelbooking } from './modelbooking';
import {
  FormGroup,
  FormBuilder,
  Validator,
  Validators,
  FormControl,
  AbstractControl,
  ValidatorFn,
} from '@angular/forms';
import { PageService } from './../../share/page.service';

import {
  MAT_MOMENT_DATE_FORMATS,
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import 'moment/locale/th';

interface District {
  value: string;
  name: string;
}

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css'],

})

export class BookingComponent implements OnInit {
  booking: Modelbooking;
  bookings!: Modelbooking[];
  form!: FormGroup;
  Departureth!:Date
  Arrivalth!:Date
  check=false;
  check2=false;
  checkdate=new Date();

  list: District[] = [
    { value: '01-THAI', name: 'Thailand' },
    { value: '02-JAP', name: 'Japan' },
    { value: '03-U.S.A', name: 'United States' },
    { value: '04-ENG', name: 'England' },
    { value: '05-INDO', name: 'Indonesia' },
    { value: '06-LAOS', name: 'Laos' },
    { value: '07-GER', name: 'Germany' },
    { value: '08-FIN', name: 'Finland' },
    { value: '09-FRA', name: 'France' },
    { value: '10-DEN', name: 'Denmark' },
    { value: '11-SWE', name: 'Sweden' },
  ];

  constructor(
    private fb: FormBuilder,
    private pageService: PageService,
  ) {
    this.booking = new Modelbooking(
      '',
      '',
      '',
      '',
      new Date(),
      new Date(),
      0,
      0,
      0
    );
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      Name: ['', Validators.required],
      from: ['', Validators.required],
      to: ['', Validators.required],
      type: ['', Validators.required],
      departure: ['', Validators.required],
      arrival: ['', Validators.required],
      adults: [0,Validators.pattern('^(d*[1-9])$')],
      children: [0,Validators.pattern('^(d*[0-9])$')],
      infants: [0,Validators.pattern('^(d*[0-9])$')],
    });
    this.bookings = this.pageService.getBooks();
  }

  onsubmit(f: FormGroup): void {
    this.booking.fullName = f.get('name')?.value;
    this.booking.from = f.get('from')?.value;
    this.booking.to = f.get('to')?.value;
    this.booking.type = f.get('type')?.value;
    this.booking.departure = f.get('departure')?.value;
    this.booking.arrival = f.get('arrival')?.value;
    this.booking.adults = f.get('adults')?.value;
    this.booking.children = f.get('children')?.value;
    this.booking.infants = f.get('infants')?.value;

    if(this.booking.from===this.booking.to){
      this.check=true
    }
    else if(this.booking.from!=this.booking.to){
      this.check=false
    }
    // if(this.booking.departure<this.booking.arrival && this.booking.departure>this.checkdate){
    //   this.check2=true
    // }
    // else{
    //   this.check2=false
    // }

    if(this.booking.adults>0 && this.check===false){
      let form_record = new Modelbooking(
        f.get('Name')?.value,
        f.get('from')?.value,
        f.get('to')?.value,
        f.get('type')?.value,
        f.get('departure')?.value.toLocaleDateString('th-TH'),
        f.get('arrival')?.value.toLocaleDateString('th-TH'),
        f.get('adults')?.value,
        f.get('children')?.value,
        f.get('infants')?.value
    );

      this.pageService.addBooks(form_record);
  }
  }
}

